import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

// export class UpdateUserDto extends PartialType(CreateUserDto) {}

export class UpdateUserDto {
    email: string;
    fullName: string;
    gender: string;
    password: string;
    roles: string;
    image: string;
}