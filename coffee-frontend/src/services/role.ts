import type { Role } from '@/types/Role'
import http from './http'

function addRole(Role: Role) {
  return http.post('/Roles', Role)
}

function updateRole(Role: Role) {
  return http.patch(`/Roles/${Role.id}`, Role)
}

function delRole(Role: Role) {
  return http.delete(`/Roles/${Role.id}`)
}

function getRole(id: number) {
  return http.get(`/Roles/${id}`)
}

function getRoles() {
  return http.get('/Roles')
}

export default { addRole, updateRole, delRole, getRole, getRoles }
