import router from '@/router'
import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}
instance.interceptors.request.use(
  async function (config) {
    console.log('request interceptors')
    const token = localStorage.getItem('access_token')
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    return config
  },
  function (error) {
    console.log(error.response.status)
    if (error.response.status === 401) {
      router.replace('/login')
    }
    return Promise.reject(error)
  }
)
instance.interceptors.response.use(
  async function (res) {
    console.log('reponse intercepters')
    await delay(1)
    return res
  },
  function (error) {
    return Promise.reject(error)
  }
)
export default instance
